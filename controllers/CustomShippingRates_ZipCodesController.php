<?php

namespace Craft;

/**
 * Zip codes Controller
 *
 * Defines actions which can be posted to by forms in our templates.
 */
class CustomShippingRates_ZipCodesController extends BaseController
{

    protected $allowAnonymous = array('actionCheckZipcode');


    /**
     * Save Zip code
     *
     * Create or update an existing zip code, based on POST data
     */
    public function actionSaveZipCode()
    {
        $this->requirePostRequest();

        if ($id = craft()->request->getPost('zipcodeId')) {
            $model = craft()->customShippingRates_zipCode->getZipCodeById($id);
        } else {
            $model = craft()->customShippingRates_zipCode->newZipCode($id);
        }

        $attributes = craft()->request->getPost('zipcode');

      //  throw new Exception(json_encode($attributes));

        $model->setAttributes($attributes);

        if (craft()->customShippingRates_zipCode->saveZipCode($model)) {

            craft()->userSession->setNotice(Craft::t('Zip code saved.'));

            $this->redirect('/admin/customshippingrates/zipcodes');

        } else {

            craft()->userSession->setError(Craft::t("Couldn't save zip code."));

            $this->redirect('/admin/customshippingrates/zipcodes');
        }

    }

    /**
     * Delete Zip code
     *
     * Delete an existing zip code
     */
    public function actionDeleteZipCode()
    {
        $this->requirePostRequest();
        $this->requireAjaxRequest();

        $id = craft()->request->getRequiredPost('id');
        craft()->customShippingRates_zipCode->deleteZipCodeById($id);

        $this->returnJson(array('success' => true));
    }



    public function actionCheckZipcode()
    {
        $check = 0;

        if (! empty($_GET['zipcode'])) {

            $code = substr($_GET['zipcode'], 0,3);

            $zipCodes = craft()->customShippingRates_zipCode->getAllZipCodes();

            foreach ($zipCodes as $arr){
               if($arr['code'] == $code){
                   $check = 1;
                   break;
               }
            }

        }

        $response = array(
            'status' => $check
        );

        $this->returnJson($response);

    }
}
