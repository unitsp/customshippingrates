# Custom Shipping Rates plugin for Craft CMS

Custom Shipping Rates

![Screenshot](resources/screenshots/plugin_logo.png)

## Installation

To install Custom Shipping Rates, follow these steps:

1. Download & unzip the file and place the `customshippingrates` directory into your `craft/plugins` directory
2.  -OR- do a `git clone ???` directly into your `craft/plugins` folder.  You can then update it with `git pull`
3.  -OR- install with Composer via `composer require /customshippingrates`
4. Install plugin in the Craft Control Panel under Settings > Plugins
5. The plugin folder should be named `customshippingrates` for Craft to see it.  GitHub recently started appending `-master` (the branch name) to the name of the folder for zip file downloads.

Custom Shipping Rates works on Craft 2.4.x and Craft 2.5.x.

## Custom Shipping Rates Overview

-Insert text here-

## Configuring Custom Shipping Rates

-Insert text here-

## Using Custom Shipping Rates

-Insert text here-

## Custom Shipping Rates Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Josh](pluginfactory.io)
