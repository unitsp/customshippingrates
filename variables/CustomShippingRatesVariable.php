<?php
/**
 * Custom Shipping Rates plugin for Craft CMS
 *
 * Custom Shipping Rates Variable
 *
 * --snip--
 * Craft allows plugins to provide their own template variables, accessible from the {{ craft }} global variable
 * (e.g. {{ craft.pluginName }}).
 *
 * https://craftcms.com/docs/plugins/variables
 * --snip--
 *
 * @author    Josh
 * @copyright Copyright (c) 2017 Josh
 * @link      pluginfactory.io
 * @package   CustomShippingRates
 * @since     1
 */

namespace Craft;

class CustomShippingRatesVariable
{
    protected $ratesRecord;

    function __construct()
    {
        $this->ratesRecord = CustomShippingRates_RatesRecord::model();
    }

    public function getRates()
    {
        /*$option = array();
        $criteria=new \CDbCriteria();
        if(!empty($memberId)){
            $option['member_id'] = $memberId;
            $criteria->addCondition('member_id ="'.$memberId.'"');

        }*/
//        $requests = $this->ratesRecord->findAll($criteria);
        $requests = $this->ratesRecord->findAll();

        return $requests;
    }



    public function getRate($id)
    {
        $rate = $this->ratesRecord->findById($id);
        return $rate;
    }

    public function getRateByZip($zip)
    {
        $rate = $this->ratesRecord->findByAttributes(array( 'zip_code' => $zip) ,'', array());
        return $rate;
    }


    /**
     * Get all available zip codes
     *
     * @return array
     */
    public function getAllZipCodes()
    {
        return craft()->customShippingRates_zipCode->getAllZipCodes();
    }

    /**
     * Get a specific zip code. If no zip codes is found, returns null
     *
     * @param  int   $id
     * @return mixed
     */
    public function getZipCodeById($id)
    {
        return craft()->customShippingRates_zipCode->getZipCodeById($id);
    }

}
