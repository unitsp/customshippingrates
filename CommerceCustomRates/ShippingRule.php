<?php

namespace CustomShippingRates;

use Commerce\Interfaces\ShippingRule as CommerceShippingRule;
use function Craft\craft;
use Craft\Exception;

class ShippingRule implements CommerceShippingRule
{
    private $_description;
    private $_price;
    private $_order;
    private $_rate;

    /**
     * ShippingRule constructor.
     *
     * @param      $carrier
     * @param      $service
     * @param      $order
     */
    public function __construct($carrier, $service, \Craft\Commerce_OrderModel $order = null)
    {

        $this->_description = $service['name'];
        $this->_order = $order;
        if ($order  && !is_null($order) && is_object($order->getShippingAddress())) {
            $zipDigit = substr($order->getShippingAddress()->zipCode, 0, 1);
            $this->_rate = craft()->customShippingRates_main->getRateByZip($zipDigit);
            $price = 0;

            $sizes = array();
            $products = $order->getLineItems();

            foreach ($products as $key=>$item) {
                $size = substr($item->getShippingCategory(), -1);
                $sizes[$key] = $size;
            }
            
            arsort($sizes);

            $charges = array(1,0.66,0.5);

            foreach ($sizes as $key=>$shipSize){

                $quantity = $products[$key]->qty;

                for ($i=0; $i < $quantity ; $i++){

                    $percent = 0.33;

                    if (!empty($charges)){
                        $percent = array_shift($charges);
                    }

                    $charge = $this->_rate[$service['handle'].$shipSize];

                    $price += $percent*$charge;
                }

            }

            $this->_price = $price;

        }

    }


    /**
     * Is this rule a match on the order? If false is returned, the shipping engine tries the next rule.
     *
     * @return bool
     */
    public function matchOrder(\Craft\Commerce_OrderModel $order)
    {
        return true;
    }

    /**
     * Is this shipping rule enabled for listing and selection
     *
     * @return bool
     */
    public function getIsEnabled()
    {
        return true;
    }

    /**
     * Stores this data as json on the orders shipping adjustment.
     *
     * @return mixed
     */
    public function getOptions()
    {
        return [];
    }

    /**
     * Returns the percentage rate that is multiplied per line item subtotal.
     * Zero will not make any changes.
     *
     * @return float
     */
    public function getPercentageRate()
    {
        return 0.00;
    }

    /**
     * Returns the flat rate that is multiplied per qty.
     * Zero will not make any changes.
     *
     * @return float
     */
    public function getPerItemRate()
    {
        return 0.00;
    }

    /**
     * Returns the rate that is multiplied by the line item's weight.
     * Zero will not make any changes.
     *
     * @return float
     */
    public function getWeightRate()
    {
        return 0.00;
    }

    /**
     * Returns a base shipping cost. This is added at the order level.
     * Zero will not make any changes.
     *
     * @return float
     */
    public function getBaseRate()
    {
        return (float)$this->_price;
    }

    /**
     * Returns a max cost this rule should ever apply.
     * If the total of your rates as applied to the order are greater than this, the baseShippingCost
     * on the order is modified to meet this max rate.
     *
     * @return float
     */
    public function getMaxRate()
    {
        return 0.00;
    }

    /**
     * Returns a min cost this rule should have applied.
     * If the total of your rates as applied to the order are less than this, the baseShippingCost
     * on the order is modified to meet this min rate.
     * Zero will not make any changes.
     *
     * @return float
     */
    public function getMinRate()
    {
        return 0.00;
    }

    /**
     * Returns a description of the rates applied by this rule;
     * Zero will not make any changes.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->_description;
    }
}
