<?php
/**
 * Created by PhpStorm.
 * User: unit
 * Date: 20.11.17
 * Time: 14:13
 */

namespace Craft;


class CustomShipingRates_RatesModel extends BaseModel
{
    protected function defineAttributes()
    {
        return array(
            'zip_code'     => array(AttributeType::Number),
            'ground1'     => array('type' => AttributeType::Number, 'decimals' => 2,),
            'ground2'     => array('type' => AttributeType::Number, 'decimals' => 2,),
            'ground3'     => array('type' => AttributeType::Number, 'decimals' => 2,),
            'second_day_air1'     => array('type' => AttributeType::Number, 'decimals' => 2,),
            'second_day_air2'     => array('type' => AttributeType::Number, 'decimals' => 2,),
            'second_day_air3'     => array('type' => AttributeType::Number, 'decimals' => 2,),
            'next_day_air1'     => array('type' => AttributeType::Number, 'decimals' => 2,),
            'next_day_air2'     => array('type' => AttributeType::Number, 'decimals' => 2,),
            'next_day_air3'     => array('type' => AttributeType::Number, 'decimals' => 2,),
        );
    }
}