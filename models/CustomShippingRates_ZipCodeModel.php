<?php
/**
 * Created by PhpStorm.
 * User: unit
 * Date: 20.11.17
 * Time: 14:13
 */

namespace Craft;


class CustomShippingRates_ZipCodeModel extends BaseModel
{
    protected function defineAttributes()
    {
        return array(
            'id'    => AttributeType::Number,
            'code' => array(AttributeType::String),
        );
    }
}