<?php
/**
 * Custom Shipping Rates plugin for Craft CMS
 *
 * Custom Shipping Rates Translation
 *
 * @author    Josh
 * @copyright Copyright (c) 2017 Josh
 * @link      pluginfactory.io
 * @package   CustomShippingRates
 * @since     1
 */

return array(
    'Translate me' => 'To this',
);
