<?php
/**
 * Custom Shipping Rates plugin for Craft CMS
 *
 * Custom Shipping Rates
 *
 * --snip--
 * Craft plugins are very much like little applications in and of themselves. We’ve made it as simple as we can,
 * but the training wheels are off. A little prior knowledge is going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL, as well as some semi-
 * advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://craftcms.com/docs/plugins/introduction
 * --snip--
 *
 * @author    Joshua Beers
 * @copyright Copyright (c) 2017 Josh
 * @link      pluginfactory.io
 * @package   CustomShippingRates
 * @since     1
 */

namespace Craft;
use CustomShippingRates\ShippingMethod;
use CustomShippingRates\ShippingRule;

require __DIR__.'/CommerceCustomRates/ShippingMethod.php';
require __DIR__.'/CommerceCustomRates/ShippingRule.php';

class CustomShippingRatesPlugin extends BasePlugin
{
    /**
     * Called after the plugin class is instantiated; do any one-time initialization here such as hooks and events:
     *
     * craft()->on('entries.saveEntry', function(Event $event) {
     *    // ...
     * });
     *
     * or loading any third party Composer packages via:
     *
     * require_once __DIR__ . '/vendor/autoload.php';
     *
     * @return mixed
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Returns the user-facing name.
     *
     * @return mixed
     */
    public function getName()
    {
         return Craft::t('Shipping Rates');
    }

    /**
     * Plugins can have descriptions of themselves displayed on the Plugins page by adding a getDescription() method
     * on the primary plugin class:
     *
     * @return mixed
     */
    public function getDescription()
    {
        return Craft::t('Custom Shipping Rates');
    }

    /**
     * Plugins can have links to their documentation on the Plugins page by adding a getDocumentationUrl() method on
     * the primary plugin class:
     *
     * @return string
     */
    public function getDocumentationUrl()
    {
        return '???';
    }

    /**
     * Plugins can now take part in Craft’s update notifications, and display release notes on the Updates page, by
     * providing a JSON feed that describes new releases, and adding a getReleaseFeedUrl() method on the primary
     * plugin class.
     *
     * @return string
     */
    public function getReleaseFeedUrl()
    {
        return '???';
    }

    /**
     * Returns the version number.
     *
     * @return string
     */
    public function getVersion()
    {
        return '1';
    }

    /**
     * As of Craft 2.5, Craft no longer takes the whole site down every time a plugin’s version number changes, in
     * case there are any new migrations that need to be run. Instead plugins must explicitly tell Craft that they
     * have new migrations by returning a new (higher) schema version number with a getSchemaVersion() method on
     * their primary plugin class:
     *
     * @return string
     */
    public function getSchemaVersion()
    {
        return '1';
    }

    /**
     * Returns the developer’s name.
     *
     * @return string
     */
    public function getDeveloper()
    {
        return 'Josh';
    }

    /**
     * Returns the developer’s website URL.
     *
     * @return string
     */
    public function getDeveloperUrl()
    {
        return 'pluginfactory.io';
    }

    /**
     * Returns whether the plugin should get its own tab in the CP header.
     *
     * @return bool
     */
    public function hasCpSection()
    {
        return true;
    }

    /**
     * Called right before your plugin’s row gets stored in the plugins database table, and tables have been created
     * for it based on its records.
     */
    public function onBeforeInstall()
    {
    }

    /**
     * Called right after your plugin’s row has been stored in the plugins database table, and tables have been
     * created for it based on its records.
     */
    public function onAfterInstall()
    {
        $zipcodes = array(
            array('code' => '001'),
            array('code' => '006'),
            array('code' => '007'),
            array('code' => '008'),
            array('code' => '009'),
            array('code' => '090'),
            array('code' => '091'),
            array('code' => '092'),
            array('code' => '093'),
            array('code' => '094'),
            array('code' => '095'),
            array('code' => '096'),
            array('code' => '097'),
            array('code' => '098'),
            array('code' => '099'),
            array('code' => '962'),
            array('code' => '963'),
            array('code' => '964'),
            array('code' => '965'),
            array('code' => '966'),
            array('code' => '967'),
            array('code' => '968'),
            array('code' => '969'),
            array('code' => '995'),
            array('code' => '996'),
            array('code' => '997'),
            array('code' => '998'),
            array('code' => '999'),
        );

        $rates = array(
            array('zip_code' => 0, 'ground1' => 15.00 , 'ground2' => 29.00 , 'ground3' => 53.00 , 'second_day_air1' => 44.00 , 'second_day_air2' => 70.00 , 'second_day_air3' => 168.00 , 'next_day_air1' => 73.00 , 'next_day_air2' => 128.00 , 'next_day_air3' => 330.00),
            array('zip_code' => 1, 'ground1' => 15.00 , 'ground2' => 29.00 , 'ground3' => 53.00 , 'second_day_air1' => 44.00 , 'second_day_air2' => 70.00 , 'second_day_air3' => 168.00 , 'next_day_air1' => 73.00 , 'next_day_air2' => 128.00 , 'next_day_air3' => 330.00),
            array('zip_code' => 2, 'ground1' => 15.00 , 'ground2' => 29.00 , 'ground3' => 53.00 , 'second_day_air1' => 44.00 , 'second_day_air2' => 70.00 , 'second_day_air3' => 168.00 , 'next_day_air1' => 73.00 , 'next_day_air2' => 128.00 , 'next_day_air3' => 330.00),
            array('zip_code' => 3, 'ground1' => 15.00 , 'ground2' => 29.00 , 'ground3' => 53.00 , 'second_day_air1' => 44.00 , 'second_day_air2' => 70.00 , 'second_day_air3' => 168.00 , 'next_day_air1' => 73.00 , 'next_day_air2' => 128.00 , 'next_day_air3' => 330.00),
            array('zip_code' => 4, 'ground1' => 12.00 , 'ground2' => 18.00 , 'ground3' => 28.00 , 'second_day_air1' => 22.00 , 'second_day_air2' => 36.00 , 'second_day_air3' => 72.00 , 'next_day_air1' => 42.00 , 'next_day_air2' => 66.00 , 'next_day_air3' => 128.00),
            array('zip_code' => 5, 'ground1' => 12.00 , 'ground2' => 18.00 , 'ground3' => 28.00 , 'second_day_air1' => 22.00 , 'second_day_air2' => 36.00 , 'second_day_air3' => 72.00 , 'next_day_air1' => 42.00 , 'next_day_air2' => 66.00 , 'next_day_air3' => 128.00),
            array('zip_code' => 6, 'ground1' => 12.00 , 'ground2' => 18.00 , 'ground3' => 28.00 , 'second_day_air1' => 22.00 , 'second_day_air2' => 36.00 , 'second_day_air3' => 72.00 , 'next_day_air1' => 42.00 , 'next_day_air2' => 66.00 , 'next_day_air3' => 128.00),
            array('zip_code' => 7, 'ground1' => 15.00 , 'ground2' => 29.00 , 'ground3' => 53.00 , 'second_day_air1' => 44.00 , 'second_day_air2' => 70.00 , 'second_day_air3' => 168.00 , 'next_day_air1' => 73.00 , 'next_day_air2' => 128.00 , 'next_day_air3' => 330.00),
            array('zip_code' => 8, 'ground1' => 15.00 , 'ground2' => 29.00 , 'ground3' => 53.00 , 'second_day_air1' => 44.00 , 'second_day_air2' => 70.00 , 'second_day_air3' => 168.00 , 'next_day_air1' => 73.00 , 'next_day_air2' => 128.00 , 'next_day_air3' => 330.00),
            array('zip_code' => 9, 'ground1' => 15.00 , 'ground2' => 29.00 , 'ground3' => 53.00 , 'second_day_air1' => 44.00 , 'second_day_air2' => 70.00 , 'second_day_air3' => 168.00 , 'next_day_air1' => 73.00 , 'next_day_air2' => 128.00 , 'next_day_air3' => 330.00),
        );



        foreach ($zipcodes as $zipcode) {
            craft()->db->createCommand()->insert('customshippingrates_zipcodes', $zipcode);
        }

        foreach ($rates as $rate) {
            craft()->db->createCommand()->insert('customshippingrates', $rate);
        }
    }

    /**
     * Called right before your plugin’s record-based tables have been deleted, and its row in the plugins table
     * has been deleted.
     */
    public function onBeforeUninstall()
    {
    }

    /**
     * Called right after your plugin’s record-based tables have been deleted, and its row in the plugins table
     * has been deleted.
     */
    public function onAfterUninstall()
    {
    }

    public function registerCpRoutes()
    {
        return array(
            'customshippingrates/edit/(?P<rateId>\d+)' => 'customshippingrates/ratesTable/_edit',
            'customshippingrates/zipcodes' => 'customshippingrates/zipCodes/',
            'customshippingrates/zipcodes/(?P<zipcodeId>\d+)' => 'customshippingrates/zipCodes/_edit',
            'customshippingrates/zipcodes/new' => 'customshippingrates/zipCodes/_edit',
        );
    }

    public function commerce_registerShippingMethods(\Craft\Commerce_OrderModel $order = null)
    {
        $shippingMethods = [];
        if ($order){

            $itemsNumber = 0;
            $monitorsNumber = 0;
            $casesNumber = 0;

            foreach ($order->getLineItems() as $item){
                $cat = $item->getShippingCategory();
                if($cat->handle == 'monitor' ){
                    $itemsNumber+=$item->qty;
                    $monitorsNumber+=$item->qty;
                }else if( $cat->handle == 'case' ){
                    $itemsNumber+=$item->qty;
                    $casesNumber+=$item->qty;
                }
            }
            //if two or more only ground
            if ($itemsNumber > 0){
                //if($itemsNumber >= 3 || ($casesNumber > 1 && $monitorsNumber >1 && $itemsNumber == 2)){
                //1 monitor + 1 case
                //1 monitor + 0 case
                //0 monitor + 1 case

                if ($itemsNumber <=2 && $monitorsNumber <=1 && $casesNumber <=1) {
                  $shippingMethods[] = new ShippingMethod('UPS', [ "handle" => "ground", "name" => 'Ground'], $order);
                  $shippingMethods[] = new ShippingMethod('UPS', [ "handle" => "second_day_air", "name" => 'Second Day Air'], $order);
                  $shippingMethods[] = new ShippingMethod('UPS', [ "handle" => "next_day_air", "name" => 'Next Day Air'], $order);
                }else{
                  $shippingMethods[] = new ShippingMethod('UPS', [ "handle" => "ground_only", "name" => 'Ground'], $order);
                }

            }else{

                foreach ($order->getLineItems() as $item){
                    $cat = $item->getShippingCategory();
                    if($cat->handle == 'accessories'){
                        return [new ShippingMethod('UPS', [ "handle" => "second_day_air_only", "name" => 'Second Day Air'], $order)];
                    }
                }

                $shippingMethods[] = new ShippingMethod('UPS', [ "handle" => "ground", "name" => 'Ground'], $order);
                $shippingMethods[] = new ShippingMethod('UPS', [ "handle" => "second_day_air", "name" => 'Second Day Air'], $order);
                $shippingMethods[] = new ShippingMethod('UPS', [ "handle" => "next_day_air", "name" => 'Next Day Air'], $order);
            }
        }else{

            $shippingMethods[] = new ShippingMethod('UPS', [ "handle" => "ground", "name" => 'Ground']);
            $shippingMethods[] = new ShippingMethod('UPS', [ "handle" => "second_day_air", "name" => 'Second Day Air']);
            $shippingMethods[] = new ShippingMethod('UPS', [ "handle" => "next_day_air", "name" => 'Next Day Air']);

        }
        return $shippingMethods;
    }

}
