<?php
/**
 * Custom Shipping Rates plugin for Craft CMS
 *
 * CustomShippingRates_Main Service
 *
 * --snip--
 * All of your plugin’s business logic should go in services, including saving data, retrieving data, etc. They
 * provide APIs that your controllers, template variables, and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 * --snip--
 *
 * @author    Josh
 * @copyright Copyright (c) 2017 Josh
 * @link      pluginfactory.io
 * @package   CustomShippingRates
 * @since     1
 */

namespace Craft;

class CustomShippingRates_ZipCodeService extends BaseApplicationComponent
{
    protected $zipCodeRecord;

    /**
     * Create a new instance of the Zip codes Service.
     * Constructor allows requestRecord dependency to be injected to assist with unit testing.
     *
     * @param @requestRecord requestRecord The zip code record to access the database
     */
    public function __construct($zipCodeRecord = null)
    {
        $this->zipCodeRecord = $zipCodeRecord;
        if (is_null($this->zipCodeRecord)) {
            $this->zipCodeRecord = CustomShippingRates_ZipCodeRecord::model();
        }
    }

    /**
     * Get a new blank for quoted Zip Code
     *
     * @param  array $attributes
     * @return CustomShippingRates_ZipCodeModel
     */
    public function newZipCode($attributes = array())
    {
        $model = new CustomShippingRates_ZipCodeModel();
        $model->setAttributes($attributes);

        return $model;
    }

    /**
     * Get all zip codes from the database.
     *
     * @return array
     */
    public function getAllZipCodes()
    {
        $records = $this->zipCodeRecord->findAll('', array());

        return $records;
    }

    /**
     * Get a specific zip code from the database based on ID. If no zip codes exists, null is returned.
     *
     * @param  int   $id
     * @return mixed
     */
    public function getZipCodeById($id)
    {
     //   $record  = $this->zipCodeRecord->findById($id);
        if ($record = $this->zipCodeRecord->findByPk($id)) {
            return CustomShippingRates_ZipCodeModel::populateModel($record);
        }
    }

    /**
     * Save a new or existing zip code back to the database.
     *
     * @param  CustomShippingRates_ZipCodeModel $model
     * @return bool
     */
    public function saveZipCode(CustomShippingRates_ZipCodeModel &$model)
    {
        if ($id = $model->getAttribute('id')) {

            if (null === ($record = $this->zipCodeRecord->findByPk($id))) {
                throw new Exception(Craft::t('Can\'t find zip code with ID "{id}"', array('id' => $id)));
            }

        } else {

            $record = $this->zipCodeRecord->create();
        }

        $record->setAttributes($model->getAttributes());
        if ($record->save()) {
            // update id on model (for new records)
            $model->setAttribute('id', $record->getAttribute('id'));

            return true;
        } else {
            $model->addErrors($record->getErrors());

            return false;
        }
    }

    /**
     * Delete an zip code from the database.
     *
     * @param  int $id
     * @return int The number of rows affected
     */
    public function deleteZipCodeById($id)
    {
        return $this->zipCodeRecord->deleteByPk($id);
    }

}
