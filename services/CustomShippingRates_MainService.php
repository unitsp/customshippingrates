<?php
/**
 * Custom Shipping Rates plugin for Craft CMS
 *
 * CustomShippingRates_Main Service
 *
 * --snip--
 * All of your plugin’s business logic should go in services, including saving data, retrieving data, etc. They
 * provide APIs that your controllers, template variables, and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 * --snip--
 *
 * @author    Josh
 * @copyright Copyright (c) 2017 Josh
 * @link      pluginfactory.io
 * @package   CustomShippingRates
 * @since     1
 */

namespace Craft;

class CustomShippingRates_MainService extends BaseApplicationComponent
{
    protected $ratesRecord;

    /**
     * Create a new instance of the Cocktail Recpies Service.
     * Constructor allows requestRecord dependency to be injected to assist with unit testing.
     *
     * @param @requestRecord requestRecord The ingredient record to access the database
     */
    public function __construct($ratesRecord = null)
    {
        $this->ratesRecord = $ratesRecord;
        if (is_null($this->ratesRecord)) {
            $this->ratesRecord = CustomShippingRates_RatesRecord::model();
        }
    }

    public function editRate($rate_id, $data)
    {
        $rate = $this->ratesRecord->findById($rate_id);

        foreach ($data as $key => $value) {
            $rate->setAttribute($key, $value);
        }
        $rate->save();
        return 'ok';
        
    }

    public function getRate($rate_id)
    {
        $rate = $this->ratesRecord->findById($rate_id);
        return $rate;
    }

    public function getRateByZip($zip)
    {
        $zip = intval($zip);
        $rate = $this->ratesRecord->findByAttributes(array( 'zip_code' => $zip) ,'', array());

        return $rate;
    }

}
